fun main() {
    println("Ingrese un numero entero: ")
    var numeroSolicitado = readln().toInt()
    val numero = numeroSolicitado
    var numeroParaQueSeaDivisible: Int
    var multiploDe7: Int

    numeroParaQueSeaDivisible = 0
    multiploDe7 = 7

    if (numeroSolicitado % multiploDe7 == 0) {
        println("El numero es multiplo de 7")
    } else {
        println("El numero no es multiplo de 7")
        while (numeroSolicitado % multiploDe7 != 0) {
            numeroParaQueSeaDivisible++
            numeroSolicitado++
        }
        println("A $numero hay que sumarle $numeroParaQueSeaDivisible para ser multiplo de 7")
    }
}