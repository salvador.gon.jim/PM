fun main() {
    println("Introduce una nota de 1 a 10: ")
    val nota = readln().toInt()
    when (nota) {
        in 1..4 -> print("Insuficiente")
        5 -> print("Suficiente")
        6 -> print("Bien")
        in 7..8 -> print("Notable")
        in 9..10 -> print("Sobresaliente")
    }
}