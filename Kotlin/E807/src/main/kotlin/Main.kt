fun main() {
    var listaNumeros = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    var contadorPositivos = 0
    var contadorNegativos = 0
    var contadorPares = 0
    var contadorMultiplos = 0
    for (i in 1 until 11) {
        println("Introduce el $i numero")
        var numero = readln().toInt()
        listaNumeros[i-1] = numero
    }
    for (i in 0 until listaNumeros.size) {
        if (listaNumeros[i] > 0) {
            contadorPositivos++
        }
        if (listaNumeros[i] < 0) {
            contadorNegativos++
        }
        if (listaNumeros[i] % 2 == 0) {
            contadorPares++
        }
        if (listaNumeros[i] % 5 == 0) {
            contadorMultiplos++
        }
    }
    println("Hay $contadorPositivos numeros positivos")
    println("Hay $contadorNegativos numeros negativos")
    println("Hay $contadorPares numeros pares")
    println("Hay $contadorMultiplos numeros multiplos de 5")
}