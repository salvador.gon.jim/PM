fun main() {
    print("Ingresa un numero: ")
    val numero = readln().toInt()
    if (numero%2 == 0){
        println("El numero es par")
    } else {
        println("El numero es impar")
    }
}