fun main() {

    val pesoPersona1: Float
    val pesoPersona2: Float
    val pesoPersona3: Float

    pesoPersona1 = 50F
    pesoPersona2 = 65F
    pesoPersona3 = 70F

    val mediaPesos = (pesoPersona1+pesoPersona2+pesoPersona3)/3

    println("La media de los pesos es $mediaPesos")

}