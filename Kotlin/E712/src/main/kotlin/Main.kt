fun main() {
    println("Introduce el dia: ")
    val diaIntroducido = readln().toInt()
    println("Introduce el mes: ")
    val mesIntroducido = readln().toInt()
    println("Introduce el año: ")
    val añoIntroducido = readln().toInt()
    var diasDelMes: Int = 0;
    when (mesIntroducido) {
        1, 3, 5, 7, 8, 10, 12 -> diasDelMes = 31
        4, 6, 9, 11 -> diasDelMes = 30
        2 -> diasDelMes = 28
    }
    var fechaCorrecta = false
    if (mesIntroducido > 0 && mesIntroducido < 13) {
        if (diaIntroducido > 0 && diaIntroducido <= diasDelMes) {
            fechaCorrecta = true
        }
    }
    if (fechaCorrecta) {
        var diaSiguiente = diaIntroducido
        var mesSiguiente = mesIntroducido
        var añoSiguiente = añoIntroducido
        if (diaIntroducido == diasDelMes) {
            diaSiguiente = 1
            if (mesIntroducido == 12) {
                mesSiguiente = 1
                añoSiguiente++
            } else {
                mesSiguiente++
            }
        } else {
            diaSiguiente++
        }
        println("$diaIntroducido/$mesIntroducido/$añoIntroducido")
        println("$diaSiguiente/$mesSiguiente/$añoSiguiente")
    } else {
        println("La fecha introducida no es válida.")
    }
}