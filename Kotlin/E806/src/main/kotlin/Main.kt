fun main() {
    var primerCuadrante = 0
    var segundoCuadrante = 0
    var tercerCuadrante = 0
    var cuartoCuadrante = 0
    var origen = 0
    println("Ingresa la cantidad de cordenadas a analizar: ")
    val n = readln().toInt()
    var i = 0
    while (i != n) {
        println("Ingresa la cordenada x: ")
        val x = readln().toInt()
        println("Ingresa la cordenada y: ")
        val y = readln().toInt()
        if (x > 0 && y > 0) {
            primerCuadrante++
        } else if (x < 0 && y > 0) {
            segundoCuadrante++
        } else if (x < 0 && y < 0) {
            tercerCuadrante++
        } else if (x > 0 && y < 0) {
            cuartoCuadrante++
        } else {
            origen++
        }
        i++
    }
    println("El primer cuadrante tiene $primerCuadrante cordenadas")
    println("El segundo cuadrante tiene $segundoCuadrante cordenadas")
    println("El tercer cuadrante tiene $tercerCuadrante cordenadas")
    println("El cuarto cuadrante tiene $cuartoCuadrante cordenadas")
}