fun main() {
    println("¿Cuanto cuesta el articulo?")
    val precioArticulo = readln().toInt()
    println("¿Cuantos articulos te llevas?")
    val numArticulos = readln().toInt()
    val precioTotal = precioArticulo*numArticulos
    println("Debes abonar $precioTotal")
}