fun main() {
    val numeroCinco = 5
    val numeroMayor = 10
    var resultado: Int
    var numero = 1
    println("Tabla del $numeroCinco")
    while (numeroMayor >= numero){
        resultado = numeroCinco * numero
        println("$numeroCinco x $numero = $resultado")
        numero++
    }
}