fun main() {
    println("Introduce un numero: ")
    val numero = readln().toInt()
    if (numero <= 0) {
        println("El numero debe ser mayor que cero")
    } else {
        println("Numeros primos entre 1 y $numero:")
        for (i in 1..numero) {
            var esPrimo = true
            if (i < 1) {
                esPrimo = false
            } else {
                var j = 2
                while (j < i) {
                    if (i % j == 0) {
                        esPrimo = false
                    }
                    j++
                }
            }
            if (esPrimo) {
                println("$i -> primo")
            } else {
                println("$i -> no primo")
            }
        }
    }
}