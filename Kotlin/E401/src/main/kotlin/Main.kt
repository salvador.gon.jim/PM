fun main() {
    print("Ingrese la medida del lado del cuadrado:")
    val lado = readln().toInt()
    val superficie = lado * 2
    val perimetro = lado * 4
    println("La superficie del cuadrado es $superficie")
    println("El perímetro del cuadrado es $perimetro")
}