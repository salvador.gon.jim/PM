open class Hora(hora: Int, minuto: Int) {
    var hora: Int = hora
    var minuto: Int = minuto
    open fun inc() {
        minuto++
        if (minuto == 60) {
            minuto = 0
            hora++
            if (hora == 24) {
                hora = 0
            }
        }
    }
    fun setMinutos(minutosIntroducidos: Int): Boolean {
        var esValido : Boolean = false
        if (minutosIntroducidos in 0..59) {
            minuto = minutosIntroducidos
            esValido = true
        }
        return esValido
    }
    fun setHora(horaIntroducida: Int): Boolean {
        var esValido : Boolean = false
        if (horaIntroducida in 0..23) {
            hora = horaIntroducida
            esValido = true
        }
        return esValido
    }
    override fun toString(): String {
        return "$hora:$minuto"
    }
}

class HoraExacta(hora: Int, minuto: Int, segundo: Int) : Hora(hora, minuto) {
    private var segundo: Int = segundo
    fun setSegundo(segundoIntroducido: Int): Boolean {
        var esValido : Boolean = false
        if (segundoIntroducido in 0..59) {
            segundo = segundoIntroducido
            esValido = true
        }
        return esValido
    }
    override fun inc() {
        segundo++
        if (segundo == 60) {
            segundo = 0
        }
        super.inc()
    }
    override fun toString(): String {
        return "$hora:$minuto:$segundo"
    }
}

fun main() {
    val miHoraExacta = HoraExacta(10, 30, 59)
    println("Hora actual")
    println(miHoraExacta.toString())
    println("Metodo inc() y mostar")
    miHoraExacta.inc()
    println(miHoraExacta.toString())
    println("Metodo setSegundo() y mostar")
    miHoraExacta.setSegundo(30)
    println(miHoraExacta.toString())
    println("Estableciendo minutos no validos y mostrar")
    println(miHoraExacta.setSegundo(75))
    println(miHoraExacta.toString())
}
