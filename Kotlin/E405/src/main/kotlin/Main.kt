fun main() {
    var totalCentimetros: Int
    println("Introduce los milimetros: ")
    val milimetros = readln().toInt()

    println("Introduce los centimetros: ")
    val centimetros = readln().toInt()

    println("Introduce los metros: ")
    val metros = readln().toInt()

    totalCentimetros = (milimetros/10)+centimetros+(metros*100)

    println("Son un total de $totalCentimetros centimetros")

}