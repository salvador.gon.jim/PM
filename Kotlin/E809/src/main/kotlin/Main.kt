fun main() {
    println("Introduce la hora")
    val hora = readln().toInt()
    println("Introduce los minutos")
    val minutos = readln().toInt()
    println("Introduce los segundos")
    val segundos = readln().toInt()
    val incrementoEnSegundos = 10
    var segundosTotal = hora*3600 + minutos*60 + segundos + incrementoEnSegundos
    val horaIncrementada = segundosTotal/3600
    val minutosIncrementados = (segundosTotal%3600)/60
    val SegundosIncrementados = (segundosTotal%3600)%60
    println("La hora incrementada es $horaIncrementada:$minutosIncrementados:$SegundosIncrementados")
}