fun main() {
    println("Introduce un número de filas: ")
    val numeroFilas = readln().toInt()
    var filaAnterior = intArrayOf(1)
    for (i in 1..numeroFilas) {
        val filaActual = IntArray(i)
        for (j in 0 until i) {
            if (j == 0 || j == (i - 1)) {
                filaActual[j] = 1
            } else {
                filaActual[j] = filaAnterior[j] + filaAnterior[j - 1]
            }
            print("${filaActual[j]} ")
        }
        filaAnterior = filaActual
        println()
    }
}