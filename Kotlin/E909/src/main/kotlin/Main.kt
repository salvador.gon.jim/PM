fun main() {
    println("Introduce un numero: ")
    val numero = readln().toInt()
    muestraPares(numero)
}
fun muestraPares(numero: Int) {
    for (i in 1..numero) {
        if (i % 2 == 0) {
            println(i)
        }
    }
}