fun main() {
    println("Introduce el número de días")
    val numeroDias = readln().toInt()
    println("Introduce el número de horas")
    val numeroHoras = readln().toInt()
    println("Introduce el número de minutos")
    val numeroMinutos = readln().toInt()
    muestraSegundos(numeroDias, numeroHoras, numeroMinutos)
}
fun muestraSegundos(numeroDias: Int,  numeroHoras: Int, numeroMinutos: Int) {
    val numeroSegundos = numeroDias * 24 * 60 * 60 + numeroHoras * 60 * 60 + numeroMinutos * 60
    println(numeroSegundos)
}