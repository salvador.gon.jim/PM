class Hora(hora: Int, minuto: Int) {
    private var hora: Int = hora
    private var minuto: Int = minuto
    fun inc() {
        minuto++
        if (minuto == 60) {
            minuto = 0
            hora++
            if (hora == 24) {
                hora = 0
            }
        }
    }
    fun setMinutos(minutosIntroducidos: Int): Boolean {
        var esValido : Boolean = false
        if (minutosIntroducidos in 0..59) {
            minuto = minutosIntroducidos
            esValido = true
        }
        return esValido
    }
    fun setHora(horaIntroducida: Int): Boolean {
        var esValido : Boolean = false
        if (horaIntroducida in 0..23) {
            hora = horaIntroducida
            esValido = true
        }
        return esValido
    }
    override fun toString(): String {
        return "$hora:$minuto"
    }
}
fun main() {
    val miHora = Hora(10, 30)
    println("Hora actual")
    println(miHora.toString())
    println("Metodo inc() y mostrar")
    miHora.inc()
    println(miHora.toString())
    println("Establecer hora y mostrar")
    println(miHora.setHora(12))
    println(miHora.toString())
    println("Establecer minutos y mostrar")
    println(miHora.setMinutos(34))
    println(miHora.toString())
    println("Estableciendo minutos no validos y mostrar")
    println(miHora.setMinutos(75))
    println(miHora.toString())
    println("Estableciendo hora no valida y mostrar")
    println(miHora.setHora(33))
    println(miHora.toString())
}

