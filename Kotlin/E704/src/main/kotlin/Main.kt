fun main() {
    print("Ingresa un numero entre 1 y 99: ")
    val numero = readln().toInt()
    val digitos = numero.toString().length
    if (numero > 99 && numero < 1){
        println("El numero no esta comprendido entre 1 y 99")
    } else {
        println("El numero contiene $digitos digitos")
    }
}