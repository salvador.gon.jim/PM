fun main() {
    print("Ingresa un numero decimal: ")
    val numero = readln().toDouble()
    if(numero > -1 && numero < 1 && numero.compareTo(0) != 0){
        println("El numero es casi 0")
    } else{
        println("El numero NO es casi 0")
    }
}