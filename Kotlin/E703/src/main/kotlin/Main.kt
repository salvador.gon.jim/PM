fun main() {
    print("Ingresa la primera nota: ")
    val nota1 = readln().toInt()
    print("Ingresa la segunda nota: ")
    val nota2 = readln().toInt()
    print("Ingresa la tercera nota: ")
    val nota3 = readln().toInt()

    val notaMedia : Int = (nota1+nota2+nota3)/3
    if (notaMedia >= 7){
        println("Promocionado")
    } else {
        println("No promocionado")
    }
}