fun main() {
    val baseTriangulo = 4
    for (i in 0 until baseTriangulo) {
        for (k in 0 until baseTriangulo - i - 1) {
            print(" ")
        }
        for (j in 0..i) {
            print(" *")
        }
        println()
    }
}