fun main() {

    print("Ingresa un numero entero entre 0 y 99999: ")
    val numero = readln().toInt()
    if (numero > 0 && numero > 99999){
        println("El numero no esta comprendido entre el rango indicado")
    } else {
        val cifras = numero.toString().length
        println("El numero contiene $cifras cifras")
    }
}