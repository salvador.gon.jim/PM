fun main() {

    val enero = 1
    val febrero = 2
    val marzo = 3
    val abril = 4
    val mayo = 5
    val junio = 6
    val julio = 7
    val agosto = 8
    val septiembre = 9
    val octubre = 10
    val noviembre = 11
    val diciembre = 12

    println("Introduce el dia: ")
    val diaIntroducido = readln().toInt()
    println("Introduce el mes: ")
    val mesIntroducido = readln().toInt()
    println("Introduce el año: ")
    val añoIntroducido = readln().toInt()
    var fechaCorrecta = false

    if (diaIntroducido <= 28 && mesIntroducido == febrero) {
        fechaCorrecta = true
    } else if (diaIntroducido <= 30 && mesIntroducido == enero || diaIntroducido <= 30 && mesIntroducido == marzo || diaIntroducido <= 30 && mesIntroducido
        == abril || diaIntroducido <= 30 && mesIntroducido == mayo || diaIntroducido <= 30 && mesIntroducido == junio || diaIntroducido <= 30 && mesIntroducido
        == julio || diaIntroducido <= 30 && mesIntroducido == agosto || diaIntroducido <= 30 && mesIntroducido == septiembre || diaIntroducido <= 30 &&
        mesIntroducido == octubre || diaIntroducido <= 30 && mesIntroducido == noviembre || diaIntroducido <= 30 && mesIntroducido == diciembre
    ) {
        fechaCorrecta = true
    } else if (diaIntroducido <= 31 && mesIntroducido == enero || diaIntroducido <= 31 && mesIntroducido == marzo || diaIntroducido <= 31 && mesIntroducido
        == mayo || diaIntroducido <= 31 && mesIntroducido == julio || diaIntroducido <= 31 && mesIntroducido == agosto || diaIntroducido <= 31 && mesIntroducido
        == octubre || diaIntroducido <= 31 && mesIntroducido == diciembre
    ) {
        fechaCorrecta = true
    }

    if (fechaCorrecta) {
        println("La fecha es correcta $diaIntroducido:$mesIntroducido:$añoIntroducido")
    } else {
        println("La fecha es incorrecta")
    }
}