fun main() {
    print("Ingresa un numero: ")
    val numero1 = readln().toInt()
    print("Ingresa un numero: ")
    val numero2 = readln().toInt()
    print("Ingresa un numero: ")
    val numero3 = readln().toInt()

    println("Los numeros ingresados son de mayor a menor: ")
    if (numero1 >= numero2 && numero2 >= numero3) {
        println("$numero1 >= $numero2 >= $numero3")
    } else if (numero1 >= numero3 && numero3 >= numero2) {
        println("$numero1 >= $numero3 >= $numero2")
    } else if (numero2 >= numero1 && numero1 >= numero3) {
        println("$numero2 >= $numero1 >= $numero3")
    } else if (numero2 >= numero3 && numero3 >= numero1) {
        println("$numero2 >= $numero3 >= $numero1")
    } else if (numero3 >= numero1 && numero1 >= numero2) {
        println("$numero3 >= $numero1 >= $numero2")
    } else if (numero3 >= numero2 && numero2 >= numero1) {
        println("$numero3 >= $numero2 >= $numero1")
    }
}