fun main() {
    println("Introduce un número: ")
    val numero = readln().toInt()
    divisoresPrimos(numero)
}
fun divisoresPrimos(numero: Int) {
    if (numero <= 0) {
        println("El número debe ser mayor que cero.")
    } else {
        println("Divisores primos de $numero:")
        for (i in 1..numero) {
            var esPrimo : Boolean
            if (numero < 1) {
                esPrimo = false
            }
            for (i in 2 until numero) {
                if (numero % i == 0) {
                    esPrimo = false
                }
            }
            esPrimo = true
            if (numero % i == 0 && esPrimo) {
                println(i)
            }
        }
    }
}


