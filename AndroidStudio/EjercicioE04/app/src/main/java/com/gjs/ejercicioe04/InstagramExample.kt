package com.gjs.ejercicioe04

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import java.util.regex.Pattern

@OptIn(ExperimentalMaterial3Api::class)
@Preview(
    name = "P1",
    showBackground = true,
    fontScale = 1.1f,
    showSystemUi = true,
    apiLevel = 34,
    device = Devices.NEXUS_6
)
@Composable
fun InstagramExample() {
    val colorFacebook = 0xFF0398FB
    var email by rememberSaveable { mutableStateOf("") }
    var password by rememberSaveable { mutableStateOf("") }
    var passwordVisible by rememberSaveable { mutableStateOf(false) }
    var enabled by rememberSaveable { mutableStateOf(true) }

    fun enableLogin(email: String, password: String): Boolean {
        val emailPattern = Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+")
        return emailPattern.matcher(email).matches() && password.length > 6
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Icon(
            imageVector = Icons.Rounded.Close,
            contentDescription = "Close",
            tint = Color.Black,
            modifier = Modifier
                .size(40.dp)
                .clickable { System.exit(0); }
                .align(Alignment.End)
        )
        Image(
            painter = painterResource(id = R.drawable.insta),
            contentDescription = "instaIMG",
            modifier = Modifier
                .padding(top = 10.dp)
                .align(Alignment.CenterHorizontally)
                .size(150.dp)
        )
        OutlinedTextField(
            value = email,
            onValueChange = { email = it },
            placeholder = {
                Text(
                    text = "Email address",
                    color = Color.Gray
                )
            },
            modifier = Modifier
                .padding(top = 10.dp)
                .height(50.dp)
                .width(360.dp)
                .align(Alignment.CenterHorizontally)
        )
        OutlinedTextField(
            value = password,
            onValueChange = { password = it },
            singleLine = true,
            placeholder = {
                Text(
                    text = "Password",
                    color = Color.Gray
                )
            },
            modifier = Modifier
                .padding(top = 10.dp)
                .height(50.dp)
                .width(360.dp)
                .align(Alignment.CenterHorizontally),
            visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
            trailingIcon = {
                val image = if (passwordVisible)
                    Icons.Filled.Visibility
                else Icons.Filled.VisibilityOff

                val description = if (passwordVisible) "Hide password" else "Show password"

                IconButton(onClick = { passwordVisible = !passwordVisible }) {
                    Icon(imageVector = image, description)
                }
            }
        )
        Text(
            "Forgotten password?",
            fontWeight = FontWeight.Bold,
            color = Color(colorFacebook),
            modifier = Modifier
                .padding(end = 25.dp, top = 10.dp)
                .align(Alignment.End)
        )
        Button(
            modifier = Modifier
                .padding(top = 20.dp)
                .height(50.dp)
                .width(360.dp)
                .align(Alignment.CenterHorizontally),
            shape = RoundedCornerShape(5.dp),
            onClick = { enabled = false },
            enabled = enableLogin(email = email, password = password),
            colors = ButtonDefaults.buttonColors(
                contentColor = Color.White,
                containerColor = Color(colorFacebook)
            )
        ) {
            Text(
                text = "Log In"
            )
        }
        Row(
            modifier = Modifier
                .padding(top = 20.dp)
                .width(360.dp)
                .align(Alignment.CenterHorizontally)
        ) {
            Divider(
                color = Color.Gray,
                modifier = Modifier
                    .height(1.dp)
                    .width(150.dp)
                    .align(Alignment.CenterVertically)
            )
            Text(
                text = "OR",
                fontWeight = FontWeight.Bold,
                color = Color.Gray,
                modifier = Modifier
                    .padding(horizontal = 20.dp)
            )
            Divider(
                color = Color.Gray,
                modifier = Modifier
                    .height(1.dp)
                    .width(150.dp)
                    .align(Alignment.CenterVertically)
            )
        }
        Row(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .padding(top = 30.dp, bottom = 100.dp)
        ) {
            Column {
                Image(
                    modifier = Modifier
                        .size(20.dp),
                    painter = painterResource(id = R.drawable.fb),
                    contentDescription = "facebookIMG"
                )
            }
            Column {
                Text(
                    " Log in with Facebook",
                    fontWeight = FontWeight.Bold,
                    color = Color(colorFacebook)
                )
            }
        }
        Divider(
            color = Color.Gray,
            modifier = Modifier
                .fillMaxWidth()
                .height(1.dp)
        )
        val styles = listOf(
            AnnotatedString.Range(SpanStyle(Color.Gray), 0, 23),
            AnnotatedString.Range(SpanStyle(Color(colorFacebook)), 23, 30),
        )
        Text(AnnotatedString("Don't have an account? Sign Up", styles),
            modifier= Modifier
                .padding(top = 10.dp)
                .align(Alignment.CenterHorizontally))

    }
}
