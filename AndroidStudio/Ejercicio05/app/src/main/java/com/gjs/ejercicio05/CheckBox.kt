package com.gjs.ejercicio05

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Checkbox
import androidx.compose.material3.CheckboxColors
import androidx.compose.material3.CheckboxDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

//1.Envolvemos los parametros del Checkbox en una clase de datos llamada Option
//2. Creamos una funcion componible llamada Checkboxlist que recibe una lista de opciones y el titulo asociado
//3. creamos una columa para desplegar verticalmente los elementos

data class Option(
    var checked: Boolean,
    var onCheckedChange: (Boolean) -> Unit = {},
    var label: String,
    var enabled: Boolean = true
)

@Composable
fun LabelledCheckbox(
    cheked: Boolean,
    label: String,
    enabled: Boolean = true,
    colors: CheckboxColors = CheckboxDefaults.colors(),
    modifier: Modifier = Modifier,
    onCheckedChange: (Boolean) -> Unit
) {
    Row(
        modifier = Modifier.height(48.dp),
        verticalAlignment = Alignment.CenterVertically,
        ) {
        Checkbox(
            modifier = modifier,
            checked = cheked,
            onCheckedChange = onCheckedChange,
            enabled = enabled,
            colors = colors
        )
        Spacer(modifier = Modifier.width(15.dp))
        Text(text = label)
    }
}


@Composable
fun CheckBoxList(options: List<Option>, listTitle: String) {
    Column {
        Text(listTitle, textAlign = TextAlign.Justify)
        Spacer(Modifier.size(16.dp))
        options.forEach { option ->
            LabelledCheckbox(
                cheked = option.checked,
                label =  option.label,
            onCheckedChange = option.onCheckedChange,
                    enabled = option.enabled,
            )
        }
    }
}

@Preview(
    name = "P1",
    showBackground = true,
    fontScale = 1.1f,
    showSystemUi = true,
    apiLevel = 33,
    device = Devices.NEXUS_6,
    backgroundColor = 10000000
)
@Composable
fun CheckBoxListExample() { //Ejecutar esta!!
    val ingredientes = listOf("Tomate", "Cebolla", "Ketchup", "Mostaza")

    //El map recorre la lista de Strings y devuelve la lista de optios
    val options = ingredientes.map {
        var checked by rememberSaveable { mutableStateOf(false) }
        //Este es el constructor de option
        Option(
            checked = checked,
            onCheckedChange = { checked = it },
            label = it
        )
    }
    CheckBoxList(options = options, listTitle = "¿Como quiere la hambuerguesa?")
}

@Composable
fun MyCheckBoxWithText() {
    var state by rememberSaveable {
        mutableStateOf(false)
    }
    Row(
        modifier = Modifier.height(18.dp),
        verticalAlignment = Alignment.CenterVertically,

        ) {
        Checkbox(
            checked = state,
            onCheckedChange = { state = !state },
            enabled = true,
            colors = CheckboxDefaults.colors(
                checkedColor = Color.Red,
                uncheckedColor = Color.Blue,
                checkmarkColor = Color.Green
            )
        )
        Spacer(modifier = Modifier.width(15.dp))
        Text(text = "Label")
    }
}

@Composable
fun CheckboxLabelExample() {
    var checked by rememberSaveable {
        mutableStateOf(false)
    }
    LabelledCheckbox(
        cheked = checked,
        label = "Checkbox con etiqueta",
        onCheckedChange = { checked = it })
}

@Composable
fun MyCheckBox() {
    var state by rememberSaveable {
        mutableStateOf(false)
    }
    Checkbox(
        checked = state,
        onCheckedChange = { state = !state },
        enabled = true,
        colors = CheckboxDefaults.colors(
            checkedColor = Color.Red,
            uncheckedColor = Color.Blue,
            checkmarkColor = Color.Green
        )
    )
}



