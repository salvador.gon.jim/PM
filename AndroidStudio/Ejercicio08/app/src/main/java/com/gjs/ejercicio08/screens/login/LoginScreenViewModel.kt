package com.gjs.ejercicio08.screens.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.Firebase
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import kotlinx.coroutines.launch

class LoginScreenViewModel : ViewModel() {


    private val auth: FirebaseAuth = Firebase.auth //La estaremos usando a lo largo del proyecto

    //impide que se creen variuos usuarios accidentalmente
    private val _loading = MutableLiveData(false)

    fun singInWithEmailAndPassword(email: String, password: String, home: () -> Unit) =
        viewModelScope.launch {
            try {
                auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d("MyLogin", "singInWithEmailAndPassword logueado!!")
                            home()
                        } else {
                            Log.d(
                                "MyLogin",
                                "singInWithEmailAndPassword: ${task.result.toString()} "
                            )
                        }
                    }
            } catch (ex: Exception) {
                Log.d("MyLogin", "singInWithEmailAndPassword: ${ex.message} ")
            }
        }

    fun createWithEmailAndPassword(email: String, password: String, home: () -> Unit) {
        if (_loading.value == false) {
            _loading.value = true
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val displayName =
                        //task.result.user?.email?.split("@")
                        //  ?.get(0) //Extrae el username para el firestone
                            //createUser(displayName)
                            home()
                    } else {
                        Log.d(
                            "MyLogin",
                            "singInWithEmailAndPassword: ${task.result.toString()} "
                        )

                    }
                    _loading.value = false
                }
        }
    }
    fun signInWithGoogleCredntial(credential: AuthCredential, home: () -> Unit) =
        viewModelScope.launch {
            auth.signInWithCredential(credential).addOnCompleteListener{task ->
                try {
                    if (task.isSuccessful) {
                        Log.d("MyLogin", "logueado!!")
                        home()
                    } else {
                        Log.d(
                            "MyLogin",
                            "singInWithEmailAndPassword: ${task.result.toString()} "
                        )
                    }
                } catch(ex: Exception) {
                    Log.d("MyLogin", "singInWithEmailAndPassword: ${ex.message} ")
                }
            }
        }

    //private fun createUser(displayName: String?) {
    //}
}
