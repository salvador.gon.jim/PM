import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument

import com.gjs.ejercicio06.model.Routes

@Preview(
    name = "P1",
    showBackground = true,
    fontScale = 1.1f,
    showSystemUi = true,
    apiLevel = 33,
    device = Devices.NEXUS_6
)
@Composable
fun MyNavigation() {
    val navigationController = rememberNavController()
    NavHost(
        navController = navigationController,
        startDestination = Routes.Pantalla1.route
    ) {
        composable(Routes.Pantalla1.route) {
            Screen1(navigationController)
        }

        composable(Routes.Pantalla2.route) {
            Screen2(navigationController)
        }

        composable(Routes.Pantalla3.route) {
            Screen3(navigationController)
        }

        composable(
            Routes.Pantalla4.route,
            arguments = listOf(navArgument("age"){ type= NavType.IntType })
        ) {
                backStackEntry->
            Screen4(
                navigationController,
                backStackEntry.arguments?.getInt("age") ?: 0
            )
        }
        composable(
            Routes.Pantalla5.route,
            arguments = listOf(navArgument("name"){ defaultValue = "ValorPorDefecto" })
        ) {
                backStackEntry->
            Screen5(
                navigationController,
                backStackEntry.arguments?.getString("name")
            )
        }
    }
}



@Composable
fun Screen1(navigationController: NavHostController) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Cyan)
    ) {
        Text(text = "Pantalla 1",
            modifier = Modifier
                .align(Alignment.Center)
                .clickable { navigationController.navigate(Routes.Pantalla2.route) }
        )
    }
}

@Composable
fun Screen2(navigationController: NavHostController) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Blue)
    ) {
        Text(text = "Pantalla 2",
            modifier = Modifier
                .align(Alignment.Center)
                .clickable { navigationController.navigate(Routes.Pantalla3.route) }
        )
    }
}


@Composable
fun Screen3(navigationController: NavHostController) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Green)
    ) {
        Text(text = "Pantalla 3",
            modifier = Modifier
                .align(Alignment.Center)
                .clickable { navigationController.navigate(Routes.Pantalla4.createRoute(29)) }
        )
    }
}

@Composable
fun Screen4(navigationController: NavHostController,age:Int) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Magenta)
    ) {
        Text(text = "Tengo $age años",
            modifier = Modifier
                .align(Alignment.Center)
                //.clickable { navigationController.navigate(Routes.Pantalla5.route)
                .clickable { navigationController.navigate("pantalla5")
                }
        )
    }
}

@Composable
fun Screen5(navigationController: NavHostController, name: String?) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Green)
    ) {
        Text(text = "Me llamo $name",
            modifier = Modifier
                .align(Alignment.Center)
                .clickable { navigationController.navigate(Routes.Pantalla1.route) }
        )
    }
}