package com.gjs.ejercicio09.util

data class UserData(
    var userId: String = "",
    var name: String = "",
    var profession: String = "",
    var age: Int = 0
)