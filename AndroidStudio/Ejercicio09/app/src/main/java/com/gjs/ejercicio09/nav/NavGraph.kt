package com.gjs.ejercicio09.nav

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.gjs.ejercicio09.screen.AddDataScreen
import com.gjs.ejercicio09.screen.GetDataScreen
import com.gjs.ejercicio09.screen.MainScreen
import com.gjs.ejercicio09.util.SharedViewModel

@Composable
fun NavGraph(
    navController: NavHostController,
    sharedViewModel: SharedViewModel
){
    NavHost(navController = navController,
        startDestination = Screens.MainScreen.route){
        //Main Screen
        composable(
            route = Screens.MainScreen.route
        ){
            MainScreen(
                navController = navController
            )
        }
        //Get data Screen
        composable(
            route = Screens.GetDataScreen.route
        ){
            GetDataScreen(
                navController = navController,
                sharedViewModel = sharedViewModel
            )
        }
        //Add data Screen
        composable(
            route = Screens.AddDataScreen.route
        ){
            AddDataScreen(
                navController = navController,
                sharedViewModel = sharedViewModel
            )
        }
    }
}