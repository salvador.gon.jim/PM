package com.gjs.ejercicio09.util

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.google.firebase.firestore.firestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.toObject
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SharedViewModel() : ViewModel() {
    fun saveData(
        userData: UserData,
        context : android.content.Context
    ) = CoroutineScope(Dispatchers.IO).launch{
        val fireStoreRef = Firebase.firestore
            .collection("user")
            .document(userData.userId)
        try {
            fireStoreRef.set(userData)
                .addOnSuccessListener {
                    Toast.makeText(context, "Succefully saved Data.", Toast.LENGTH_SHORT).show()
                }
        } catch (e: Exception){
            Toast.makeText(context, "Succefully saved Data.", Toast.LENGTH_SHORT).show()
        }

    }
    fun retriveData(
        userId: String,
        context: android.content.Context,
        data: (UserData)->Unit
    )=CoroutineScope(Dispatchers.IO).launch {
        val fireStoreRef= Firebase.firestore
            .collection("user")
            .document(userId)
        try {
            fireStoreRef.get()
                .addOnSuccessListener {
                    if (it.exists()){
                        val userData = it.toObject<UserData>()
                        if (userData != null) {
                            data(userData)
                        } else {
                            Toast.makeText(context, "No User Data Found", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
        }catch (e: Exception){
            Toast.makeText(context,e.message,Toast.LENGTH_SHORT).show()
        }
    }

    fun deleteData(
        userId: String,
        context: android.content.Context,
        navController: NavController
    ) {
        val fireStoreRef = Firebase.firestore
            .collection("user")
            .document(userId)

        try {
            fireStoreRef.delete()
                .addOnSuccessListener {
                    Toast.makeText(context, "Successfully delete Data", Toast.LENGTH_SHORT).show()
                    navController.popBackStack()
                }
        } catch (e: Exception) {
            Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
        }
    }
}