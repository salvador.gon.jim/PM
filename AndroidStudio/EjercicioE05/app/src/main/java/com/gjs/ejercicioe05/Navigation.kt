package com.gjs.ejercicioe05

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.gjs.ejercicioe05.screens.Home
import com.gjs.ejercicioe05.screens.Instagram
import com.gjs.ejercicioe05.screens.SplashScreen
//OK
@Composable
fun Navigation(){
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.SplashScreen.name
    )
    {
        composable(Screens.SplashScreen.name){
            SplashScreen(navController = navController)
        }
        composable(Screens.LoginScreen.name){
            Instagram(navController = navController)
        }
        composable(Screens.HomeScreen.name){
            Home(navController = navController)
        }
    }
}