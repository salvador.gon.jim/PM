package com.gjs.ejercicio01

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.gjs.ejercicio01.ui.theme.Ejercicio01Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Ejercicio01Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    MySuperText("Android")
                }
            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

//Para una preview
@Preview(
    showBackground = true,
    fontScale = 1.5f,
    showSystemUi = false,//Muestra los marcos de la pantalla
    device = Devices.NEXUS_5,//Selecciona el movil
    apiLevel = 33 //Api necesaria ara renderizar
)

@Composable
fun GreetingPreview() {
    Ejercicio01Theme {
        MySuperText("Android")
    }
}

@Composable
fun MySuperText(name: String) {
    Text(
        text = "Hola $name!",
        Modifier
            .fillMaxSize()
            .padding(20.dp)//El dp es la unidad de medida que se usa en android, el padding es como el margin
            .clickable { }//Clickear algo
    )
}