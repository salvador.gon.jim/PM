package com.gjs.ejercicio07

import android.widget.Toast
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.gjs.ejercicio07.model.Superhero
import kotlinx.coroutines.launch

@Composable
fun SimpleRecyclerView() {
    val myList = listOf("linea1", "linea2", "linea3", "linea4")
    LazyColumn {
        item { Text(text = "Cabecera") }
        items(myList) {
            Text(text = "Esta es la ... $it")
        }
        item { Text(text = "Pie") }
    }
}

@Composable
fun SuperHeroWithSpecialControlsView() {
    val context = LocalContext.current
    var rvState = rememberLazyListState()
    val showbutton by remember { derivedStateOf { rvState.firstVisibleItemIndex > 0 } }
    val courtineScope = rememberCoroutineScope()
    Column {
        LazyColumn(
            state = rvState,
            verticalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier.weight(1f)
        ) {
            items(getSuperHeroes()) { superhero ->
                ItemHero(superHero = superhero) {
                    Toast.makeText(context, it.realName, Toast.LENGTH_SHORT).show()
                }
            }
        }
        if (showbutton) {
            Button(
                onClick = { courtineScope.launch { rvState.animateScrollToItem(0) } },
                modifier = Modifier
                    .align(Alignment.CenterHorizontally)
                    .padding(16.dp)
            )
            {
                Text(text = "Go to top")
            }
        }
    }
}


@Preview(
    name = "P1",
    showBackground = true,
    fontScale = 1.1f,
    showSystemUi = true,
    apiLevel = 34,
    device = Devices.NEXUS_6
)
@Composable
fun SuperHeroGridView() {
    val context = LocalContext.current
    LazyVerticalGrid(
        modifier = Modifier
            .background(Color.White),
        columns = GridCells.Fixed(2),
        content = {
            items(getSuperHeroes()){ superhero ->
                ItemHero(
                    superHero = superhero
                ){
                    Toast.makeText(context, it.realName, Toast.LENGTH_SHORT).show()
                }
            }
        },
        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp)
    )
}

@Composable
fun SuperHeroView() {
    val context = LocalContext.current
    LazyColumn(verticalArrangement = Arrangement.spacedBy(8.dp)) {
        items(getSuperHeroes()) { superhero ->
            ItemHero(superHero = superhero) {
                Toast.makeText(context, it.realName, Toast.LENGTH_SHORT).show()
            }
        }
    }


}

@Composable
fun ItemHero(superHero: Superhero, onItemSelected: (Superhero) -> Unit) {
    Card(
        border = BorderStroke(1.dp, Color.Yellow),
        modifier = Modifier
            .fillMaxWidth()
            .padding(1.dp)
            .clickable { onItemSelected(superHero) }
    ) {
        Column() {
            Image(
                painter = painterResource(id = superHero.photo),
                contentDescription = "SuperHero Avatar",
                modifier = Modifier.fillMaxWidth(),
                contentScale = ContentScale.Crop
            )
            Text(
                text = superHero.superHeroName,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            )
            Text(text = superHero.realName, modifier = Modifier.align(Alignment.CenterHorizontally))
            Text(
                text = superHero.publisher, modifier = Modifier
                    .align(Alignment.End)
                    .padding(8.dp), fontSize = 10.sp
            )
        }
    }
}

fun getSuperHeroes(): List<Superhero> {
    return listOf(
        Superhero("Spiderman", "Petter parker", "Marvel", R.drawable.spiderman),
        Superhero("Wolverine", "James Howlett", "Marvel", R.drawable.logan),
        Superhero("Batman", "Bruce Wayne", "Marvel", R.drawable.batman),
        Superhero("Thor", "Thor Odison", "Marvel", R.drawable.thor),
        Superhero("Flash", "Jay Garrick", "Marvel", R.drawable.flash),
        Superhero("Green Latern", "Alan Scott", "Marvel", R.drawable.green_lantern),
        Superhero("Wonder Woman", "Princess Diana", "Marvel", R.drawable.wonder_woman),
    )
}